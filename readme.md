# readme.md - README for Ssepan.Application.Core.EtoForms

## TODO

### Purpose

To encapsulate common application functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### History

6.0:
~refactor to newer C# features
~refactor to existing language types
~perform format linting

5.3:
~Update dotnet to .net8.0
~Update Eto.Forms.* to 2.8.2

5.2:
~Use new About fields
~Remove filter separator constants moved to Ssepan.Application.Core
~use ACTION_ constants for status
~set dialog titles to action, not app name
~fix trapping for new filename on save

5.1:
~use new Website property in AssemblyInfoBase, and set in AssemblyInfo for modules that will display About.

5.0:
~Use model where refactored Ssepan.Application.Core.EtoForms into several libraries. Left classes specific to EtoForms in Ssepan.Application.Core.EtoForms.

## Contact

Steve Sepan
<sjsepan@yahoo.com>
3/8/2024
