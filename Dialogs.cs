﻿using System;
using System.Diagnostics;
using System.IO;
using Ssepan.Io.Core;
using Ssepan.Utility.Core;
//using Microsoft.Data.ConnectionUI;//cannot use until Microsoft.VisualStudio.Data.dll is made redistributable by Microsoft
using System.Reflection;
using Eto.Drawing;
using Eto.Forms;

namespace Ssepan.Application.Core.EtoForms
{
	public static class Dialogs
    {
        #region Methods
        /// <summary>
        /// Get path to save data.
        /// Static. Calls Eto.Forms.SaveFileDialog.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<Window, DialogResult></param>
        /// <param name="errorMessage">ref string</param>
        public static bool GetPathForSave
        (
            ref FileDialogInfo<Window, DialogResult> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
                if
                (
                    fileDialogInfo.Filename.EndsWith(fileDialogInfo.NewFilename)
                    ||
                    fileDialogInfo.ForceDialog
                )
                {
					SaveFileDialog saveFileDialog;
					using
					(
						saveFileDialog = new SaveFileDialog()
					)
					{
						saveFileDialog.Title = fileDialogInfo.Title;//(fileDialogInfo.ForceDialog ? "Save As..." : "Save...")
																	// saveFileDialog.CheckFileExists = ?;

						//define location of file for settings by prompting user for filename.
						saveFileDialog.FileName = fileDialogInfo.Filename;

						foreach (string filter in fileDialogInfo.Filters.Split(FileDialogInfo<Window, DialogResult>.FILTER_SEPARATOR))
						{
							string[] nameAndPattern = filter.Split(FileDialogInfo<Window, DialogResult>.FILTER_ITEM_SEPARATOR);
							FileFilter fileFilter = new();
							Debug.Assert(fileFilter != null);
							fileFilter.Name = nameAndPattern[0];
							fileFilter.Extensions = [nameAndPattern[1]];
							saveFileDialog.Filters.Add(fileFilter);
						}

						saveFileDialog.Directory =
							(fileDialogInfo.InitialDirectory == default)
							?
							new Uri(fileDialogInfo.CustomInitialDirectory)
							:
							new Uri(Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator());

						DialogResult response = saveFileDialog.ShowDialog(fileDialogInfo.Parent);

						if (response != DialogResult.None)
						{
							if (response == DialogResult.Ok)
							{
								if (string.Equals(Path.GetFileName(saveFileDialog.FileName), fileDialogInfo.NewFilename, StringComparison.CurrentCultureIgnoreCase))
								{
									//user did not select or enter a name different than new; for now I have chosen not to allow that name to be used for a file.--SJS, 12/16/2005
									string messageTemp = string.Format("The name \"{0}.{1}\" is not allowed; please choose another. Settings not saved.", fileDialogInfo.NewFilename.ToLower(), fileDialogInfo.Extension.ToLower());
									MessageDialogInfo<Window, DialogResult, object, MessageBoxType, MessageBoxButtons> messageDialogInfo =
							new (
								parent: fileDialogInfo.Parent,
								modal: fileDialogInfo.Modal,
								title: fileDialogInfo.Title,
								dialogFlags: null,
								messageType: MessageBoxType.Information,
								buttonsType: MessageBoxButtons.OK,
								message: messageTemp,
								response: DialogResult.None
							);

									ShowMessageDialog
									(
										ref messageDialogInfo,
										ref errorMessage
									);

									//Forced cancel
									fileDialogInfo.Response = DialogResult.Cancel;
								}
								else
								{
									//set new filename
									fileDialogInfo.Filename = saveFileDialog.FileName;
									fileDialogInfo.Filenames = [saveFileDialog.FileName];
								}
							}
							fileDialogInfo.Response = response;
							returnValue = true;
						}
						else
						{
							//treat as Cancel
							fileDialogInfo.Response = DialogResult.Cancel;
							returnValue = true;
						}
					}
				}
                else
                {
                    fileDialogInfo.Response = DialogResult.Ok;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get path to load data.
        /// Static. Calls Eto.Forms.OpenFileDialog.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<Window, DialogResult></param>
        /// <param name="errorMessage">ref string</param>
        public static bool GetPathForLoad
        (
            ref FileDialogInfo<Window, DialogResult> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
                if (fileDialogInfo.ForceNew)
                {
                    fileDialogInfo.Filename = fileDialogInfo.NewFilename;

                    returnValue = true;
                }
                else
                {
					OpenFileDialog openFileDialog;
					using
					(
						openFileDialog = new OpenFileDialog()
					)
					{
						openFileDialog.Title = fileDialogInfo.Title;
						// fileChooserDialog.CheckFileExists = ?;

						//define location of file for settings by prompting user for filename.
						foreach (string filter in fileDialogInfo.Filters.Split(FileDialogInfo<Window, DialogResult>.FILTER_SEPARATOR))
						{
							string[] nameAndPattern = filter.Split(FileDialogInfo<Window, DialogResult>.FILTER_ITEM_SEPARATOR);
							FileFilter fileFilter = new()
							{
								Name = nameAndPattern[0],
								Extensions = [nameAndPattern[1]]
							};
							openFileDialog.Filters.Add(fileFilter);
						}

						openFileDialog.MultiSelect = fileDialogInfo.Multiselect;
						openFileDialog.FileName = fileDialogInfo.Filename;
						openFileDialog.Directory =
								(fileDialogInfo.InitialDirectory == default)
								?
								new Uri(fileDialogInfo.CustomInitialDirectory)
								:
								new Uri(Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator());

						DialogResult response = openFileDialog.ShowDialog(fileDialogInfo.Parent);

						if (response != DialogResult.None)
						{
							if (response == DialogResult.Ok)
							{
								fileDialogInfo.Filename = openFileDialog.FileName;
								fileDialogInfo.Filenames = (string[])openFileDialog.Filenames;
							}
							fileDialogInfo.Response = response;
							returnValue = true;
						}
						else
						{
							//treat as Cancel
							fileDialogInfo.Response = DialogResult.Cancel;
							returnValue = true;
						}
					}
				}
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Select a folder path.
        /// Static. Calls Eto.Forms.SelectFolderDialog.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<Window, DialogResult>. returns path in Filename property</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetFolderPath
        (
            ref FileDialogInfo<Window, DialogResult> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
				SelectFolderDialog selectFolderDialog = new()
				{
					Title = fileDialogInfo.Title,
					Directory = string.IsNullOrEmpty(fileDialogInfo.Filename) ? Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator() : fileDialogInfo.Filename
				};

				DialogResult response = selectFolderDialog.ShowDialog(fileDialogInfo.Parent);

				if (response != DialogResult.None)
                {
                    if (response == DialogResult.Ok)
                    {
                        fileDialogInfo.Filename = selectFolderDialog.Directory;
                    }
                    fileDialogInfo.Response = response;
                    returnValue = true;
                }
                else
                {
                    //treat as Cancel
                    fileDialogInfo.Response = DialogResult.Cancel;
                    returnValue = true;
                }

                selectFolderDialog.Dispose();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Shows an About dialog.
        /// Static. Calls Gtk.AboutDialog.
        /// </summary>
        /// <param name="aboutDialogInfo">ref AboutDialogInfo<Window, DialogResult, Image></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool ShowAboutDialog
        (
            ref AboutDialogInfo<Window, DialogResult, Image> aboutDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			AboutDialog aboutDialog = null;

            try
            {
				aboutDialog = new AboutDialog
				{
					Title = aboutDialogInfo.Title,
					ProgramName = aboutDialogInfo.ProgramName,
					Version = aboutDialogInfo.Version,
					Copyright = aboutDialogInfo.Copyright,
					ProgramDescription = aboutDialogInfo.Comments,
					Website = new Uri(aboutDialogInfo.Website),//must be in format "http://www.somedomain.com"
					Logo = aboutDialogInfo.Logo,
					//add'l fields compatible w/ Eto.Forms
					WebsiteLabel = aboutDialogInfo.WebsiteLabel,
					License = aboutDialogInfo.License,
					Developers = aboutDialogInfo.Developers,
					Designers = aboutDialogInfo.Designers,
					Documenters = aboutDialogInfo.Documenters
				};

				DialogResult response = aboutDialog.ShowDialog(aboutDialogInfo.Parent);
				if (response != DialogResult.None)
                {
                    //if (response == DialogResult.Ok)
                    // {

                    // }
                    aboutDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            finally
            {
                aboutDialog.Dispose();
            }

            aboutDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a printer.
        /// Static. Calls Gtk.PrintUnixDialog.
        /// </summary>
        /// <param name="printerDialogInfo">ref PrinterDialogInfo<Window, DialogResult, PrintSettings>. PrinterDialogInfo</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetPrinter
        (
            ref PrinterDialogInfo<Window, DialogResult, PrintSettings> printerDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			PrintDialog printerDialog = null;

            try
            {
                printerDialog = new PrintDialog();

				DialogResult response = printerDialog.ShowDialog(printerDialogInfo.Parent);
				if (response != DialogResult.None)
                {
                    if (response == DialogResult.Ok)
                    {
                        printerDialogInfo.Printer = printerDialog.PrintSettings;
                    }

                    printerDialogInfo.Response = response;
                    returnValue = true;
                }
                else
                {
                    //treat as Cancel
                    printerDialogInfo.Response = DialogResult.Cancel;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            finally
            {
                printerDialog.Dispose();
            }

            printerDialogInfo.BoolResult = returnValue;
            return returnValue;
        }
        /// <summary>
        /// Shows a message dialog.
        /// Static. Calls Gtk.MessageDialog.
        /// </summary>
        /// <param name="messageDialogInfo">ref MessageDialogInfo<Window, DialogResult, object, MessageBoxType, MessageBoxButtons></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool ShowMessageDialog
        (
            ref MessageDialogInfo<Window, DialogResult, object, MessageBoxType, MessageBoxButtons> messageDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
				DialogResult response = MessageBox.Show
                (
                    parent: messageDialogInfo.Parent,
                    text: messageDialogInfo.Message,
                    caption: messageDialogInfo.Title,
                    buttons: messageDialogInfo.ButtonsType,
                    type: messageDialogInfo.MessageType,
                    defaultButton: MessageBoxDefaultButton.Yes
                );
				if (response != DialogResult.None)
                {
                    //return complex responses in dialoginfo
                    messageDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            messageDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a color.
        /// Static. Calls Eto.Forms.ColorDialog.
        /// </summary>
        /// <param name="colorDialogInfo">ref ColorDialogInfo<Window, DialogResult, Color>. ColorDialogInfo</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetColor
        (
            ref ColorDialogInfo<Window, DialogResult, Color> colorDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
				ColorDialog colorDialog = new()
				{
					Color = colorDialogInfo.Color
				};

				DialogResult response = colorDialog.ShowDialog(colorDialogInfo.Parent);

				if (response != DialogResult.None)
                {
                    if (response == DialogResult.Ok)
                    {
                        colorDialogInfo.Color = colorDialog.Color;
                    }
                    colorDialogInfo.Response = response;
                    returnValue = true;
                }
                else
                {
                    //treat as Cancel
                    colorDialogInfo.Response = DialogResult.Cancel;
                    returnValue = true;
                }

                colorDialog.Dispose();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            colorDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a font descriptor.
        /// Static. Calls Eto.Forms.FontDialog.
        /// </summary>
        /// <param name="fontDialogInfo">ref FontDialogInfo<Window, DialogResult, Font></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetFont
        (
            ref FontDialogInfo<Window, DialogResult, Font> fontDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
				// fontResponse = null;
				FontDialog fontDialog = new()
				{
					Font = fontDialogInfo.FontDescription
				};

				DialogResult response = fontDialog.ShowDialog(fontDialogInfo.Parent);

				// Console.WriteLine("GetFont:response="+response.ToString());
				if (response != DialogResult.None)
                {
                    if (response == DialogResult.Ok)
                    {
                        fontDialogInfo.FontDescription = fontDialog.Font;
                    }
                    fontDialogInfo.Response = response;
                    returnValue = true;
                }
                else
                {
                    //treat as Cancel
                    fontDialogInfo.Response = DialogResult.Cancel;
                    returnValue = true;
                }

                fontDialog.Dispose();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            fontDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Perform input of connection string and provider name.
        /// Uses MS Data Connections Dialog.
        /// Note: relies on MS-LPL license and code from http://archive.msdn.microsoft.com/Connection
        /// </summary>
        /// <param name="connectionString">ref string</param>
        /// <param name="providerName">ref string</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetDataConnection
        (
            ref string connectionString,
            ref string providerName,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            //DataConnectionDialog dataConnectionDialog = default(DataConnectionDialog);
            //DataConnectionConfiguration dataConnectionConfiguration = default(DataConnectionConfiguration);

            try
            {
                //dataConnectionDialog = new DataConnectionDialog();

                //DataSource.AddStandardDataSources(dataConnectionDialog);

                //dataConnectionDialog.SelectedDataSource = DataSource.SqlDataSource;
                //dataConnectionDialog.SelectedDataProvider = DataProvider.SqlDataProvider;//TODO:use?

                //dataConnectionConfiguration = new DataConnectionConfiguration(null);
                //dataConnectionConfiguration.LoadConfiguration(dataConnectionDialog);

                //(don't) set to current connection string, because it overwrites previous settings, requiring user to click Refresh in Data Connection Dialog.
                //dataConnectionDialog.ConnectionString = connectionString;

                if (true/*DataConnectionDialog.Show(dataConnectionDialog) == DialogResult.OK*/)
                {
                    ////extract connection string
                    //connectionString = dataConnectionDialog.ConnectionString;
                    //providerName = dataConnectionDialog.SelectedDataProvider.ViewName;

                    ////writes provider selection to xml file
                    //dataConnectionConfiguration.SaveConfiguration(dataConnectionDialog);

                    ////save these too
                    //dataConnectionConfiguration.SaveSelectedProvider(dataConnectionDialog.SelectedDataProvider.ToString());
                    //dataConnectionConfiguration.SaveSelectedSource(dataConnectionDialog.SelectedDataSource.ToString());

                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
        #endregion Methods
    }
}
