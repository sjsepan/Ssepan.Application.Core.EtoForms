﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Ssepan.Utility.Core;
using Eto.Drawing;
using Eto.Forms;

namespace Ssepan.Application.Core.EtoForms
{
	/// <summary>
	/// Note: this class can be subclassed without type parameters in the client.
	/// </summary>
	/// <typeparam name="TIcon">TIcon</typeparam>
	/// <typeparam name="TSettings">TSettings</typeparam>
	/// <typeparam name="TModel">TModel</typeparam>
	/// <typeparam name="TView">TView</typeparam>
	public class FormsViewModel
    <
        TIcon,
        TSettings,
        TModel,
        TView
    > :
        ViewModelBase<TIcon>
        where TIcon : class
        where TSettings : class, ISettings, new()
        where TModel : class, IModel, new()
        where TView : Window
    {
        #region Declarations
        //public delegate bool DoWork_WorkDelegate(BackgroundWorker worker, DoWorkEventArgs e, ref string errorMessage);
        public delegate TReturn DoWork_WorkDelegate<TReturn>(BackgroundWorker worker, DoWorkEventArgs e, ref string errorMessage);

        protected static FileDialogInfo<Window, DialogResult> _settingsFileDialogInfo;
        protected Dictionary<string, TIcon> _actionIconImages;

        #endregion Declarations

        #region Constructors
        public FormsViewModel()
        {
            if (SettingsController<TSettings>.Settings == null)
            {
                SettingsController<TSettings>.New();
            }
        }

        public FormsViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, TIcon> actionIconImages,
            FileDialogInfo<Window, DialogResult> settingsFileDialogInfo
        ) :
            this()
        {
            try
            {
                //(and the delegate it contains
                if (propertyChangedEventHandlerDelegate != default)
                {
                    PropertyChanged += new PropertyChangedEventHandler(propertyChangedEventHandlerDelegate);
                }

                _actionIconImages = actionIconImages;

                _settingsFileDialogInfo = settingsFileDialogInfo;

                ActionIconImage = _actionIconImages["Save"];
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        public FormsViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, TIcon> actionIconImages,
            FileDialogInfo<Window, DialogResult> settingsFileDialogInfo,
            TView view //= default(TView) //(In VB 2010, )VB caller cannot differentiate between members which differ only by an optional param--SJS
        ) :
            this(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
                View = view;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

		#endregion Constructors
		#region Properties
		public TView View { get; set; }
		#endregion Properties

		#region Methods
		#region Menus
		#region menu feature boilerplate
		//may be used as-is, or overridden to handle differently

		public virtual void FileNew()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            MessageDialogInfo<Window, DialogResult, object, MessageBoxType, MessageBoxButtons> messageDialogInfo = null;
            string errorMessage = null;
            try
            {
                StartProgressBar
                (
                    "New" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["New"],
                    true,
                    33
                );

                if (SettingsController<TSettings>.Settings.Dirty)
                {
                    //prompt before saving
                    messageDialogInfo =
                        new MessageDialogInfo<Window, DialogResult, object, MessageBoxType, MessageBoxButtons>
                        (
                            View,//saved Window reference
                            true,
                            "Save",
                            null,
                            MessageBoxType.Question,
                            MessageBoxButtons.YesNo,
                            "Save changes?",
                            DialogResult.None
                        );
                    if (!Dialogs.ShowMessageDialog
                    (
                        ref messageDialogInfo,
                        ref errorMessage
                    ))
                    {
                        throw new ApplicationException(errorMessage);
                    }

                    switch (messageDialogInfo.Response)
                    {
						case DialogResult.Yes:
							//SAVE
							FileSaveAs();

							break;

						case DialogResult.No:
							break;

						default:
							throw new InvalidEnumArgumentException();
					}
                }

                //NEW
                if (!SettingsController<TSettings>.New())
                {
                    throw new ApplicationException(string.Format("Unable to get New settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                }

                ModelController<TModel>.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// Open object at SettingsController<TSettings>.FilePath.
        /// </summary>
        /// <param name="forceDialog">If false, just use SettingsController<TSettings>.FilePath</param>
        public virtual void FileOpen(bool forceDialog = true)
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            MessageDialogInfo<Window, DialogResult, object, MessageBoxType, MessageBoxButtons> messageDialogInfo = null;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "Open" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Open"],
                    true,
                    33
                );

                if (SettingsController<TSettings>.Settings.Dirty)
                {
                    //prompt before saving
                    messageDialogInfo =
                        new MessageDialogInfo<Window, DialogResult, object, MessageBoxType, MessageBoxButtons>
                        (
                            parent: View,//saved Window reference
                            modal: true,
                            title: "Save",
                            dialogFlags: null,
                            messageType: MessageBoxType.Question,
                            buttonsType: MessageBoxButtons.YesNo,
                            message: "Save changes?",
                            response: DialogResult.None
                        );

                    if (!Dialogs.ShowMessageDialog(ref messageDialogInfo,ref errorMessage))
                    {
                        throw new ApplicationException(errorMessage);
                    }

                    if (messageDialogInfo.Response != DialogResult.None)
                    {
                        if (messageDialogInfo.Response == DialogResult.Yes)
                        {
                            //SAVE
                            FileSave();
                        }
                        else
                        {
                            //treat as cancel
                        }
                    }
                    else
                    {
                        //treat as cancel
                    }
                }

                if (forceDialog)
                {
                    _settingsFileDialogInfo.Title = "Open";
                    _settingsFileDialogInfo.Filename = SettingsController<TSettings>.FilePath;
                    if (!Dialogs.GetPathForLoad(ref _settingsFileDialogInfo, ref errorMessage))
                    {
                        throw new ApplicationException(string.Format("GetPathForLoad: {0}", errorMessage));
                    }

                    if (_settingsFileDialogInfo.Response != DialogResult.None)
                    {
                        if (_settingsFileDialogInfo.Response == DialogResult.Ok)
                        {
                            SettingsController<TSettings>.FilePath = _settingsFileDialogInfo.Filename;
                            UpdateStatusBarMessages(StatusMessage + _settingsFileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                        }
                        else
                        {
                            //treat as cancel
                            StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                            return; //if open was cancelled
                        }
                    }
                    else
                    {
                        //treat as cancel
                            StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                            return; //if open was cancelled
                    }
                }

                //OPEN
                if (!SettingsController<TSettings>.Open())
                {
                    throw new ApplicationException(string.Format("Unable to Open settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                }

                ModelController<TModel>.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void FileSave(bool isSaveAs=false)
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "Save" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Save"],
                    true,
                    33
                );

                _settingsFileDialogInfo.Title = isSaveAs ? "Save As..." : "Save...";
                _settingsFileDialogInfo.ForceDialog = isSaveAs;
                _settingsFileDialogInfo.Modal = true;
                _settingsFileDialogInfo.Filename = SettingsController<TSettings>.FilePath;
                if (!Dialogs.GetPathForSave(ref _settingsFileDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetPathForSave: {0}", errorMessage));
                }

                if (_settingsFileDialogInfo.Response != DialogResult.None)
                {
                    if (_settingsFileDialogInfo.Response == DialogResult.Ok)
                    {
                        SettingsController<TSettings>.FilePath = _settingsFileDialogInfo.Filename;
                        UpdateStatusBarMessages(StatusMessage + _settingsFileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                }

                //SAVE
                if (!SettingsController<TSettings>.Save())
                {
                    throw new ApplicationException(string.Format("Unable to Save settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                }

                ModelController<TModel>.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void FileSaveAs()
        {
            FileSave(true);
        }

        public virtual void FilePrint()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            PrinterDialogInfo<Window, DialogResult, PrintSettings> printerDialoginfo = null;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "Print" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Print"],
                    true,
                    33
                );

                //select printer
                printerDialoginfo = new PrinterDialogInfo<Window, DialogResult, PrintSettings>
                (
                    View,
                    true,
                    "Print",
                    DialogResult.None
                );

                if (!Dialogs.GetPrinter(ref printerDialoginfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetPrinter: {0}", errorMessage));
                }

                if (printerDialoginfo.Response != DialogResult.None)
                {
                    if (printerDialoginfo.Response == DialogResult.Ok)
                    {
                        UpdateStatusBarMessages(StatusMessage + printerDialoginfo.Printer + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                }

                //PRINT
                if (Print())
                {
                    // StopProgressBar("Printed.");
                }
                else
                {
                    // StopProgressBar("Print cancelled.");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void FilePrintPreview()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Print Preview" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["PrintPreview"], 
                    true,
                    33
                );

                //TODO:select printer?

                if (PrintPreview())
                {
                    StopProgressBar(StatusMessage + ACTION_DONE);
                }
                else
                {
                    StopProgressBar(StatusMessage + ACTION_CANCELLED);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void FileExit
        (
            ref bool resultCancelQuitting
        )
        {
            string errorMessage = null;
            MessageDialogInfo<Window, DialogResult, object, MessageBoxType, MessageBoxButtons> messageDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Quit" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Quit"], 
                    true,
                    33
                );

                //prompt before quitting
                messageDialogInfo =
                    new MessageDialogInfo<Window, DialogResult, object, MessageBoxType, MessageBoxButtons>
                    (
                        parent: View,//saved Window reference
                        modal: true,
                        title: "Quit?",
                        dialogFlags: null,
                        messageType: MessageBoxType.Question,
                        buttonsType: MessageBoxButtons.YesNo,
                        message: "Are you sure you want to quit?",
                        response: DialogResult.None
                    );

                if (!Dialogs.ShowMessageDialog(ref messageDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }

                if (messageDialogInfo.Response == DialogResult.No)
                {
                    resultCancelQuitting = true;
                    StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("{0}", ex.Message);

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        public virtual void EditUndo()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Undo" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Undo"],
                    true,
                    33
                );

                if (!Undo())
                {
                    throw new ApplicationException("'Undo' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditRedo()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Redo" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Redo"],
                    true,
                    33
                );

                if (!Redo())
                {
                    throw new ApplicationException("'Redo' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditSelectAll()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Select All" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["SelectAll"],
                    true,
                    33
                );

                if (!SelectAll())
                {
                    throw new ApplicationException("'Select All' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditCut()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Cut" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Cut"],
                    true,
                    33
                );

                if (!Cut())
                {
                    throw new ApplicationException("'Cut' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditCopy()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Copy" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Copy"],
                    true,
                    33
                );

                if (!Copy())
                {
                    throw new ApplicationException("'Copy' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditPaste()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Past" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Paste"],
                    true,
                    33
                );

                if (!Paste())
                {
                    throw new ApplicationException("'Paste' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditPasteSpecial()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Past Special" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["PasteSpecial"],
                    true,
                    33
                );

                if (!PasteSpecial())
                {
                    throw new ApplicationException("'Paste Special' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditDelete()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Delete" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Delete"],
                    true,
                    33
                );

                if (!Delete())
                {
                    throw new ApplicationException("'Delete' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditFind()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Find" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Find"],
                    true,
                    33
                );

                if (!Find())
                {
                    throw new ApplicationException("'Find' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditReplace()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Replace" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Replace"],
                    true,
                    33
                );

                if (!Replace())
                {
                    throw new ApplicationException("'Replace' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditRefresh()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Refresh" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Refresh"],
                    true,
                    33
                );

                if (!Refresh())
                {
                    throw new ApplicationException("'Refresh' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditPreferences()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Preferences" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Preferences"],
                    true,
                    33
                );

                if (!Preferences())
                {
                    throw new ApplicationException("'Preferences' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void EditProperties()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Properties" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Properties"],
                    true,
                    33
                );

                if (!Properties())
                {
                    throw new ApplicationException("'Properties' error");
                }

                //TODO:when implemented, a Properties dialog can also be cancelled, so handle cancel too
                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowNewWindow()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "New Window" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["NewWindow"],
                    true,
                    33
                );

                if (!NewWindow())
                {
                    throw new ApplicationException("'New Window' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowTile()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Tile" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Tile"],
                    true,
                    33
                );

                if (!Tile())
                {
                    throw new ApplicationException("'Tile' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowCascade()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Cascade" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Cascade"],
                    true,
                    33
                );

                if (!Cascade())
                {
                    throw new ApplicationException("'Cascade' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowArrangeAll()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Arrange All" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["ArrangeAll"],
                    true,
                    33
                );

                if (!ArrangeAll())
                {
                    throw new ApplicationException("'Arrange All' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowHide()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Hide" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Hide"],
                    true,
                    33
                );

                if (!Hide())
                {
                    throw new ApplicationException("'Hide' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void WindowShow()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Show" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Show"],
                    true,
                    33
                );

                if (!Show())
                {
                    throw new ApplicationException("'Show' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void HelpContents()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Contents" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Contents"],
                    true,
                    33
                );
                //System.Windows.Forms.Help
                if (!Contents())
                {
                    throw new ApplicationException("'Help Contents' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void HelpIndex()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Index" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["Index"],
                    true,
                    33
                );
                //System.Windows.Forms.Help
                if (!Index())
                {
                    throw new ApplicationException("'Help Index' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void HelpOnlineHelp()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Online Help" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["HelpOnlineHelp"],
                    true,
                    33
                );

                //System.Windows.Forms.Help
                if (!OnlineHelp())
                {
                    throw new ApplicationException("'Online Help' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void HelpLicenceInformation()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Licence Information" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["LicenceInformation"],
                    true,
                    33
                );

                if (!LicenceInformation())
                {
                    throw new ApplicationException("'Licence Information' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual void HelpCheckForUpdates()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Check For Updates" + ACTION_IN_PROGRESS,
                    null,
                    null,//_actionIconImages["CheckForUpdates"],
                    true,
                    33
                );

                if (!CheckForUpdates())
                {
                    throw new ApplicationException("'Check For Updates' error");
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        // public virtual void HelpAbout<TAssemblyInfo>()
        //     where TAssemblyInfo :
        //     //class,
        //     AssemblyInfoBase<Window>,
        //     new()
        // {
        //     StatusMessage = string.Empty;
        //     ErrorMessage = string.Empty;
        //     TAssemblyInfo assemblyInfo = null;
        //     AboutDialogInfo<Window, DialogResult, Image> aboutDialogInfo = null;
        //     DialogResult response = DialogResult.None;
        //     string errorMessage = null;

        //     try
        //     {

        //         StartProgressBar
        //         (
        //             "About" + ACTION_IN_PROGRESS, 
        //             null, 
        //             _actionIconImages["About"], 
        //             true, 
        //             33
        //         );

        //         //assemblyInfo NOT passed in;specifically, NOT called by override of HelpAbout<TAssemblyInfo>()
        //         assemblyInfo = new TAssemblyInfo();

        //         // use GUI mode About feature
        //         aboutDialogInfo = 
        //             new AboutDialogInfo<Window, DialogResult, Image>
        //             (
        //                 parent : this.View,
        //                 response : response,
        //                 modal : true,
        //                 title : "About " + assemblyInfo.Title,
        //                 programName : assemblyInfo.Product,
        //                 version : assemblyInfo.Version,
        //                 copyright : assemblyInfo.Copyright,
        //                 comments : assemblyInfo.Description,
        //                 website : assemblyInfo.Website,
        //                 //Note:cannot access logo from loaded resources
        //                 logo : null,
        //                 websiteLabel : assemblyInfo.WebsiteLabel,
        //                 designers : assemblyInfo.Designers,
        //                 developers : assemblyInfo.Developers,
        //                 documenters : assemblyInfo.Documenters,
        //                 license : assemblyInfo.License
        //             );

        //         Debug.Assert(assemblyInfo != null);

        //         if (!Dialogs.ShowAboutDialog(ref aboutDialogInfo, ref errorMessage))
        //         {
        //             throw new ApplicationException(errorMessage);
        //         }

        // //         StopProgressBar(StatusMessage + ACTION_DONE);
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //         StopProgressBar(null, string.Format("{0}", ex.Message));
        //     }
        // }
        #endregion menu feature boilerplate

        #region menu feature implementation
        //likely to need overridden to handle feature specifics

        /// <summary>
        /// placeholder and example, but this must be overridden to actually do anything
        /// with the print dialog, such as sendign a document to the printer
        /// </summary>
        /// <returns>bool</returns>
        protected virtual bool Print()
        {
            bool returnValue = default;

            try
            {
                    returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            // finally
            // {
            // }

            return returnValue;
        }

        /// <summary>
        /// placeholder and example, but this must be overridden to actually do anything
        /// </summary>
        /// <returns>bool</returns>
        protected virtual bool PrintPreview()
        {
            bool returnValue = default;
            // string errorMessage = null;
            // PrinterDialogInfo printerDialoginfo = null;

            try
            {
                // if (Dialogs.GetPrinter(ref printerDialoginfo, ref errorMessage))
                // {
                //     Log.Write(printerDialoginfo.Printer.Name,  Log.EventLogEntryType_Error);

                     returnValue = true;
                // }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Undo()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Redo()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool SelectAll()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Cut()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Copy()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Paste()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool PasteSpecial()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Delete()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Find()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Replace()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Refresh()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Preferences()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Properties()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool NewWindow()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Tile()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Cascade()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool ArrangeAll()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Hide()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Show()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Contents()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool Index()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool OnlineHelp()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool LicenceInformation()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        protected virtual bool CheckForUpdates()
        {
            bool returnValue = default;

            try
            {
                //TODO:some part of this process may need to overridden
                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }
        #endregion menu feature implementation

        #endregion Menus

        #region Controls

        /// <summary>
        /// Handle DoWork event.
        /// </summary>
        /// <typeparam name="TReturn">TReturn</typeparam>
        /// <param name="worker">BackgroundWorker</param>
        /// <param name="e">DoWorkEventArgs</param>
        /// <param name="workDelegate">DoWork_WorkDelegate<TReturn></param>
        /// <param name="resultNullDescription">string</param>
        public virtual void BackgroundWorker_DoWork<TReturn>
        (
            BackgroundWorker worker,
            DoWorkEventArgs e,
            DoWork_WorkDelegate<TReturn> workDelegate,
            string resultNullDescription = "No result was returned."
        )
        {
            string errorMessage = default;

            try
            {
                //run process
                if (workDelegate != null)
                {
                    e.Result =
                        workDelegate
                        (
                            worker,
                            e,
                            ref errorMessage
                        );
                }

                //look for specific problem
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    throw new Exception(errorMessage);
                }

                //warn about unexpected result
                if (e.Result == null)
                {
                    throw new Exception(resultNullDescription);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //re-throw and let RunWorkerCompleted event handle and report error.
                throw;
            }
        }

        /// <summary>
        /// Handle ProgressChanged event.
        /// </summary>
        /// <param name="description">string</param>
        /// <param name="userState">object, specifically a string.</param>
        /// <param name="progressPercentage">int</param>
        public virtual void BackgroundWorker_ProgressChanged
        (
            string description,
            object userState,
            int progressPercentage
        )
        {
            string message = string.Empty;

            try
            {
                if (userState != null)
                {
                    message = userState.ToString();
                }
                UpdateProgressBar(string.Format("{0} ({1})...{2}%", description, message, progressPercentage.ToString()), progressPercentage);
                //System.Windows.Forms.Application.DoEvents();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// Handle RunWorkerCompleted event.
        /// </summary>
        /// <param name="description">string</param>
        /// <param name="worker">BackgroundWorker</param>
        /// <param name="e">RunWorkerCompletedEventArgs</param>
        /// <param name="errorDelegate">Action<Exception>. Replaces default behavior of displaying the exception message.</param>
        /// <param name="cancelledDelegate">Action. Replaces default behavior of displaying a cancellation message. Handles the display message only; differs from cancelDelegate, which handles view-level behavior not specific to this worker.</param>
        /// <param name="completedDelegate">Action. Extends default behavior of refreshing the display; execute prior to Refresh().</param>
        public virtual void BackgroundWorker_RunWorkerCompleted
        (
            string description,
            BackgroundWorker worker,
            RunWorkerCompletedEventArgs e,
            Action<Exception> errorDelegate = null,
			Action cancelledDelegate = null,
			Action completedDelegate = null
        )
        {
            try
            {
                Exception error = e.Error;
                bool isCancelled = e.Cancelled;
                object result = e.Result;

                // First, handle the case where an exception was thrown.
                if (error != null)
                {
                    if (errorDelegate != null)
                    {
                        errorDelegate(error);
                    }
                    else
                    {
                        // Show the error message
                        StopProgressBar(null, error.Message);
                    }
                }
                else if (isCancelled)
                {
                    if (cancelledDelegate != null)
                    {
                        cancelledDelegate();
                    }
                    else
                    {
                        // Handle the case where the user cancelled the operation.
                        StopProgressBar(null, "Cancelled.");

                        //if (View.cancelDelegate != null)
                        //{
                        //    View.cancelDelegate();
                        //}
                    }
                }
                else
                {
					// Operation completed successfully, so display the result.
					completedDelegate?.Invoke();

					//backgroundworker calls New/Save without UI refresh; refresh UI explicitly here.
					ModelController<TModel>.Model.Refresh();
                }

                // Do post completion operations, like enabling the controls etc.      
                View.BringToFront();

                // Inform the user we're done
                StopProgressBar(description, null);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                ////clear cancellation hook
                //View.cancelDelegate = null;
            }
        }

        // /// <summary>
        // /// 
        // /// </summary>
        // public void BindingSource_PositionChanged<TItem>(BindingSource bindingSource, EventArgs e, Action<TItem> itemDelegate)
        // {
        //     try
        //     {
        //         TItem current = (TItem)bindingSource.Current;

        //         if (current != null)
        //         {
        //             if (itemDelegate != null)
        //             {
        //                 itemDelegate(current);
        //             }
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        // /// <summary>
        // /// Handle DataError event; specifically, catch combobox cell data errors.
        // /// This allows validation message to be displayed 
        // ///  and still allows the user to fix and save the settings.
        // /// </summary>
        // /// <param name="grid"></param>
        // /// <param name="e"></param>
        // /// <param name="cancel"></param>
        // /// <param name="throwException"></param>
        // /// <param name="logException"></param>
        // public void Grid_DataError
        // (
        //     DataGridView grid, 
        //     DataGridViewDataErrorEventArgs e,
        //     bool? cancel,
        //     bool? throwException,
        //     bool logException
        // )
        // {
        //     try
        //     {
        //         if (cancel.HasValue)
        //         {
        //             //cancel to prevent default DataError dialogs
        //             e.Cancel = true;
        //         }

        //         if (throwException.HasValue)
        //         {
        //             //there are cases where you do not want to throw exception; because it will drop application immediately with only a log
        //             e.ThrowException = throwException.Value;
        //         }

        //         if (logException)
        //         {
        //             Log.Write(e.Exception, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }
        #endregion Controls

        #region Utility

        /// <summary>
        /// Manage buttons' state while processes are running.
        /// Usage:
        ///     View.permitEnabledStateXxx = ButtonEnabled(enabledFlag, View.permitEnabledStateXxx, View.cmdXxx, View.menuFileXxx, View.buttonFileXxx);
        /// </summary>
        /// <param name="enabledFlag">bool</param>
        /// <param name="permitEnabledState">bool</param>
        /// <param name="button">Button</param>
        /// <param name="menuItem">MenuItem</param>
        /// <param name="buttonItem">Button</param>
        /// <returns>bool. updated remembered state</returns>
        public bool ButtonEnabled
        (
            bool enabledFlag,
            bool permitEnabledState,
            Button button,
            MenuItem menuItem = null,
            Button buttonItem = null
        )
        {
            bool returnValue = permitEnabledState; // default(bool);
            try
            {
                if (enabledFlag)
                {
                    //ENABLING
                    //recall state
                    if (button != null)
                    {
                        button.Enabled = permitEnabledState;
                    }
                    if (menuItem != null)
                    {
                        menuItem.Enabled = permitEnabledState;
                    }
                    if (buttonItem != null)
                    {
                        buttonItem.Enabled = permitEnabledState;
                    }
                }
                else
                {
                    //DISABLING
                    //remember state
                    if (button != null)
                    {
                        returnValue = button.Enabled;
                    }
                    else if (menuItem != null)
                    {
                        returnValue = menuItem.Enabled;
                    }
                    else if (buttonItem != null)
                    {
                        returnValue = buttonItem.Enabled;
                    }

                    //disable
                    if (button != null)
                    {
                        button.Enabled = enabledFlag;
                    }
                    if (menuItem != null)
                    {
                        menuItem.Enabled = enabledFlag;
                    }
                    if (buttonItem != null)
                    {
                        buttonItem.Enabled = enabledFlag;
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
//Note:put these here for now...
        // https://www.hanselman.com/blog/how-do-you-use-systemdrawing-in-net-core
        // public static Image GetImageFromFile(string resourceItemPath)
        // {
        //     Image returnValue = null;
        //     using (FileStream pngStream = new FileStream(resourceItemPath, FileMode.Open, FileAccess.Read))
        //     {
        //         Bitmap image = new Bitmap(pngStream);
        //         returnValue = image;
        //     }
        //     return returnValue;
        // }
        public static Bitmap GetBitmapFromFile(string resourceItemPath)
        {
            Bitmap returnValue = null;
            using (FileStream pngStream = new(resourceItemPath, FileMode.Open, FileAccess.Read))
            {
                returnValue = new Bitmap(pngStream);
            }
            return returnValue;
        }
        public static Icon GetIconFromBitmap(Bitmap bitmap, int width, int height)
        {
            return bitmap.WithSize(width, height);
        }
        #endregion Utility
        #endregion Methods

    }
}
